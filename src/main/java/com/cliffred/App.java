package com.cliffred;

import com.cliffred.soap.ObjectFactory;
import com.cliffred.soap.SampleRequest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {
    public static void main(String[] args) throws IOException, JAXBException {
        SampleRequest request = new SampleRequest();
        request.setName("foo");
        Path attachment = Paths.get("settings.gradle");
        request.setAttachment(Files.readAllBytes(attachment));

        JAXBContext context = JAXBContext.newInstance(SampleRequest.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(request, System.out);
    }
}
